
function replaceAll(string, token, newtoken)
{
   while (string.indexOf(token) != -1) {
   string = string.replace(token, newtoken);
   }
   return string;
}

function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if(elem != null) {
        if(elem.createTextRange) {
            var range = elem.createTextRange();
            range.move('character', caretPos);
            range.select();
        }
        else {
            if(elem.selectionStart) {
                elem.focus();
                elem.setSelectionRange(caretPos, caretPos);
            }
            else
                elem.focus();
        }
    }
}

function formata_placa(str)
{
   var p = str.split("");
   return p[0]+p[1]+p[2]+'-'+p[3]+p[4]+p[5]+p[6];
}

function parseInfoTabelaPrincipal(tag, html)
{
   var matches = html.match(new RegExp("<p id=\""+tag+"\">(.*?)<\/p>"));
   if(matches == null)
      return '';

   return matches[1];
}

function parse_impedimentos(data)
{
   var impedimentos = data.match(/<\/h3><p>(.*?)<\/p><\/div>/);

   return impedimentos ? impedimentos[1] : '';
}

function msg_erro(msg, id)
{
   $("#"+id+" span.msg_erro").html(msg);
   $("#"+id).popup("open");
}

function valida_placa(placa)
{
   var patern = /(^[A-Za-z]{3})+[0-9]{4}/;
   return patern.test(placa) && placa.length == 7;
}

// Obtem data no formato 2013-12-31
function get_data()
{
   var d = new Date();
   var curr_date = d.getDate();
   var curr_month = d.getMonth() + 1;
   var curr_year = d.getFullYear();

   return curr_year + "-" + curr_month + "-" + curr_date;
}

/*function formata_data(data)
{
   var d = new Date(data);

   return  ('0' + (d.getDate()+1)).slice(-2) + '/'
             + ('0' + (d.getMonth()+1)).slice(-2) + '/'
             + d.getFullYear();
}*/

function loadURL(url)
{

   if(navigator.network == null) // provavelmente est� sendo aberto pelo navegador
   {
      window.open(url);
      return false;
   }

    navigator.app.loadUrl(url, { openExternal:true });
    return false;
}

function rand_string()
{
	var d = new Date();

	var curr_hour = d.getHours();
	var curr_min = d.getMinutes();
	var curr_sec = d.getSeconds();
	      
	return curr_hour+curr_min+curr_sec;
}

function conta_multas(json)
{
   return Object.keys(json).length;
}

function total_valor_multas(json)
{
   var tot = 0;
   for(var i = 0; json[i] ; i++)
   {
      tot += parseFloat( json[i]['Valor'].replace("R$", "").replace(",", ".") );
   }

   return tot;
}

// teste se esta rodando dentro da plataforma do phonegap
function eh_apk()
{
   return navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/) && window.device;
}

// testa a conex�o com a internet
// s� funciona no celular
function tah_online()
{
	// provavelmente est� sendo aberto pelo navegador ou por outro dispositivo n�o suportado
   if(typeof(navigator) == "undefined"
   	|| typeof(navigator.network) == "undefined"
		|| typeof(navigator.network.connection) == "undefined"
		|| typeof(navigator.network.connection.type) == "undefined"
	) 
      return true;

	return !(navigator.network.connection.type == Connection.NONE);
}


function validarCNPJ(cnpj) {

    cnpj = cnpj.replace(/[^\d]+/g,'');

    if(cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;

    return true;

}


function validaCpf(str){
    str = str.replace('.','');
    str = str.replace('.','');
    str = str.replace('-','');

    cpf = str;
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1)){
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais){
        numeros = cpf.substring(0,9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        numeros = cpf.substring(0,10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}


function validacpfcnpj(str)
{
   return validaCpf(str) || validarCNPJ(str);
}

function tipoPessoaSP(str)
{
   if(validaCpf(str))
      return '1';

   if(validarCNPJ(str))
      return '2';

   return 'erro';
}

$.urlParam = function(name){
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}


function acessoLiberado(callback_liberado, callback_negado)
{
    if(localStorage.getItem('email') && localStorage.getItem('cod_verificacao'))
    {
        callback_liberado();
    }
    else
    {
        sql.query("select count(*) as tot from veiculo", function(tx, rs)
        {
            if(parseInt(rs.rows.item(0)['tot']) == 0)
            {
                callback_liberado();
            }
            else
            {
                callback_negado();
            }
        });
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validaSenhaSP(senha) {
    if(senha.length < 6) {
        return false;
    }
    return true;
}

function goBack(){
    history.go(-1);
}