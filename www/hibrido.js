function consulta_hibrida(estado, placa, chassi, renavan, descricao, captcha, cpf, senha, callback)
{
   var dados = '';

    if(estado == 'mg')
    {
        dados = {
            'email': localStorage.getItem('email'),
            'placa': placa,
            'chassi': chassi,
            'captcha': captcha
        };
    }
    else if(estado == 'pe')
    {
        dados = {
            'email': localStorage.getItem('email'),
            'placa': placa
        };
    }
   else if(estado == 'sp')
   {
      dados = {
         'email': localStorage.getItem('email'),
         'renavan': renavan,
         'cpfcnpj': cpf,
         'senha': senha,
         'placa': placa
      };
   }
    else if(estado == 'ba' || estado == 'pr')
    {
       dados = {
          'email': localStorage.getItem('email'),
          'renavan': renavan,
          'captcha': captcha
       };
    }

    else if(estado == 'rs')
    {
        dados = {
            'email': localStorage.getItem('email'),
            'placa': placa,
            'renavan': renavan,
            'captcha': captcha
        };
    }
    else if(estado == 'rj')
   {
      dados = {
         'email': localStorage.getItem('email'),
         'placa': placa,
         'renavan': renavan,
         'captcha1': $("input[name=captcha1-rj]").val(),
         'captcha2': $("input[name=captcha2-rj]").val()
      };
   }


   $.ajax({
      type: 'POST',
      url: URL+'/svc.'+estado+'/veiculo.php',
      dataType: 'json',
      data: dados,
      crossDomain: true,
      //jsonpCallback: 'detran',
      success: function(json)
      {
         if(json.id_erro != 0) // deu erro
         {
            callback(json.erro, json.id_erro);
            return;
         }

         grava_dados(estado, renavan, placa, chassi, descricao, json, callback);

      }
   });
}

function grava_dados(estado, renavan, placa, chassi, descricao, json, callback)
{
    if(placa != '')
    {
        sql.query("delete from veiculo where placa = '"+placa+"'");
    }
    else
    {
        sql.query("delete from veiculo where renavan = '"+renavan+"'");
    }

    var s = squel.insert();

    s.into("veiculo");

    s.set('placa', placa ? placa : json.placa);
    s.set('renavan', renavan ? renavan : json.renavam);
    s.set('chassi', chassi ? chassi : json.chassi);
    s.set('estado', estado);
    s.set('descricao', descricao);
    s.set('data_atualizacao', get_data());

    s.set('cpf', cpf ? cpf : null);
    s.set('json', JSON.stringify(json));

    sql.query(s.toString());

    callback('', '0');
}

