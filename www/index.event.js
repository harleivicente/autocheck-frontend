﻿
// executa apenas uma vez ao iniciar
sql.open();

// inicialização das tabelas
sql.db.transaction(function (tx) {

    tx.executeSql('CREATE TABLE IF NOT EXISTS versao_app (cod_versao_db)');

    tx.executeSql('CREATE TABLE IF NOT EXISTS condutor (cnh, cpf, data_nascimento, data_habilitacao, estado, total_pontos, apelido, data_atualizacao)');

    tx.executeSql('CREATE TABLE IF NOT EXISTS veiculo (placa, chassi, renavan, cpf, descricao, estado, data_atualizacao)');

    tx.executeSql('CREATE TABLE IF NOT EXISTS usuario_sp (cpf, senha, descricao)');
});


// valida versões do DB para migração
sql.query("select cod_versao_db from versao_app", function(tx, rs)
{
    // buscando código de versão dos metadados
    versao = rs.rows.length == 0 ? 0 : parseInt(rs.rows.item(0)['cod_versao_db']);

    if(versao < 15)  // formar a primeira versao
    {
        sql.query("delete from versao_app");
        sql.query("insert into versao_app (cod_versao_db) values (15)", function(tx, rs)
        {
            sql.db.transaction(function (tx)
            {
                tx.executeSql('ALTER TABLE veiculo ADD COLUMN json');
                tx.executeSql('ALTER TABLE condutor ADD COLUMN json');

                tx.executeSql("UPDATE veiculo SET data_atualizacao = ''");
                tx.executeSql("UPDATE condutor SET data_atualizacao = ''");
            });
        });
    }

});

// evento chamado quanto a página renderiza
function pg_inicio()
{
    // carrega banners dos patrocinadores (apenas se conectado)
    banner_config();

    $("li.btn-fipe").unbind('click').click(function()
    {
        if(!tah_online())
        {
            msg_erro("Habilite a conexão com a Internet para fazer a consulta", 'popup_erro2');
        }
        else
        {
            $.mobile.changePage( "fipe.html", { transition: "none", changeHash: true });
        }


    });

    $("li.btn-seguro").unbind('click').click(function(){
        // loadURL("http://autocheck.parcerias.bidu.com.br");
        window.open("http://autocheck.parcerias.bidu.com.br", '_system');
    });
    
    $("li.btn-cotacao").unbind('click').click(function(){
        //loadURL("http://www.webmotors.com.br/WebMotors/mobile/carrosBusca/carros-busca.aspx");
        // loadURL("http://home.mercadolivre.com.br/category/MLB1744");
        window.open("http://home.mercadolivre.com.br/category/MLB1744", '_system');
    });
    
    $("li.btn-manutencao").unbind('click').click(function(){
        window.open("http://m.portoseguro.com.br/seguros/seguro-de-veiculos/seguro-de-automovel/centros-automotivos-porto-seguro", '_system');
        // loadURL("http://m.portoseguro.com.br/seguros/seguro-de-veiculos/seguro-de-automovel/centros-automotivos-porto-seguro");
    });

    $("li#veic").unbind('click').click(function(){

        acessoLiberado(
            function(){
                $.mobile.changePage("veiculo.ls.html", { transition: "none", changeHash: true });
            },
            function()
            {
                $.mobile.changePage("registro.html", { transition: "none", changeHash: true });
            }
        );
    });

    $("li#habi").unbind('click').click(function(){

        acessoLiberado(
            function(){
                $.mobile.changePage("pontuacao.ls.html", { transition: "none", changeHash: true });
            },
            function(){
                $.mobile.changePage("registro.html", { transition: "none", changeHash: true });
            }
        );

    });

}