

// evento chamado quanto a página renderiza
function pg_fipe()
{
    // carrega banners dos patrocinadores (apenas se conectado)
    banner_config();


    $("select#tipo_veiculo").change(function(){

        $("table.referencia, #referencia, .select_marca, .select_modelo").hide();
        $.mobile.showPageLoadingMsg();

        var tipo = $(this).val();
        if(!tipo)
            return;

        $.ajax({
            type: 'POST',
            url: URL + '/fipe/marca.php',
            dataType: 'json',
            data: {
                'tipo_veiculo': tipo
            },
            success: function(json)
            {
                var select = '<label for="marca">Marca</label>';

                select += '<select data-inline="true" name="marca" id="marca">';

                select += '<option value="">Selecione</option>';

                for(var i in json)
                {
                    select += '<option value="'+ json[i]['id'] +'">' + json[i]['nome'] + '</option>';

                }

                select += '</select>';
                $("div.select_marca").html(select).trigger('create');

                eventos_btns_dinamicos();

                $(".select_marca").show();

                $.mobile.hidePageLoadingMsg();
            }
        });

    });
}


function eventos_btns_dinamicos()
{
    $("select#marca").change(function(){

        $("table.referencia, #referencia, .select_modelo").hide();
        $.mobile.showPageLoadingMsg();

        var idmarca = $(this).val();
        if(!idmarca)
            return;

        $.ajax({
            type: 'POST',
            url: URL + '/fipe/modelo.php',
            dataType: 'json',
            data: {
                'idmarca': idmarca,
                'tipo': $("select#tipo_veiculo").val()
            },
            success: function(json)
            {
                var select = '<label for="modelo">Modelo</label>';

                select += '<select data-inline="true" name="modelo" id="modelo">';

                select += '<option value="">Selecione</option>';

                for(var i in json)
                {
                    select += '<option value="'+ json[i]['id'] +'">' + json[i]['nome'] + '</option>';

                }

                select += '</select>';
                $("div.select_modelo").html(select).trigger('create');

                evento_btn_modelo();

                $(".select_modelo").show();

                $.mobile.hidePageLoadingMsg();

            }
        });

    });

}


function evento_btn_modelo()
{
    $("select#modelo").change(function(){

        $.mobile.showPageLoadingMsg();

        var idmodelo = $(this).val();
        if(!idmodelo)
            return;

        $("table.referencia, #referencia").hide();
        $("div#referencia").html("");

        $.ajax({
            type: 'POST',
            url: URL + '/fipe/referencia.php',
            dataType: 'json',
            data: {
                'idmodelo': idmodelo
            },
            success: function(json)
            {
                var table='', thead = '', tbody = '';

                for(var i in json)
                {
                    thead += '<th>'+json[i]['ano_modelo']+'</th>';
                    tbody += '<td>'+json[i]['preco_medio']+'</td>';
                }

                table += '<table data-role="table" class="detalhe ui-body-d ui-shadow table-stroke">';
                table += '<thead><tr>';
                table += thead;
                table += '</tr></thead><tbody><tr>';
                table += tbody;
                table += '</tr></tbody></table><br>';


                $("div#referencia").html(table).trigger('create');
                $("table.referencia, #referencia").show();

                $.mobile.hidePageLoadingMsg();

            }
        });

    });
}