﻿
var config = false;
function banner_config()
{
   if(!config)
   {
      $.ajax({
         type: 'GET',
         url: URL+'/config.json',
         dataType: "json",
         success: function(json)
         {
            config = json;
            inicializa_banner();

            if(config['msg'].ativada && COD_VERSAO < config['msg'].versoes_abaixo)
            {
               msg_notificacao(config['msg'].conteudo);
            }

         },
         error: function()
         {
            $("div.iframe").hide(); // esconde a DIV do banner
         }
      });
   }
   else
   {
      inicializa_banner();
   }

}

function msg_notificacao(msg)
{
   alert(msg);
}

var lista_banners;
var bannner_atual;
var ind_banner;

function inicializa_banner()
{
   return; // desativando temporariamente todos os banners

   if(!config)
      return;

   $("div.iframe").show();
   lista_banners = config['banners'];

   ind_banner = Math.floor((Math.random() * Object.keys(lista_banners).length));

   if((Object.keys(lista_banners).length-1) != 0)
      bannner_atual = setInterval(muda_banner, lista_banners[ind_banner].segundos * 1000);

   monta_banner(ind_banner);
}

function monta_banner(i)
{
   $("div.iframe").css("background-image", "url("+lista_banners[i].imagem+")")
   .css("background-color", lista_banners[i].fundo);

   if(!eh_apk())
      $("div.iframe").css("cursor", "pointer");

   $("div.iframe").click(function(){

      if(lista_banners[i].url)
      {
         if(eh_apk())
         {
            loadURL(lista_banners[i].url);
         }
         else
         {
            window.open(lista_banners[i].url);
         }
      }
   });
}


function muda_banner()
{
   clearInterval(bannner_atual);

   ind_banner = ind_banner==(Object.keys(lista_banners).length-1) ? 0 : ind_banner+1;

   bannner_atual = setInterval(muda_banner, lista_banners[ind_banner].segundos * 1000);

   monta_banner(ind_banner);
}





