﻿
function add_img_mg()
{
   var url = URL+"/svc.mg/auth.php?"+rand_string();

   $("div#img_captcha_mg img").remove();
   $("div#img_captcha_mg").append('<img id="img_captcha_mg" src="'+url+'" />');

   $("div#img_captcha_mg span.loader").show();

   $("img#img_captcha_mg").one('load', function()
   {
      $("div#img_captcha_mg span.loader").hide();
   }).each(function() {
      if(this.complete)
         $(this).load();
   });
}

function add_img_sp()
{
    $("div#img_captcha_sp img").remove();
    $("div#img_captcha_sp span.loader").show();
   
    $.ajax({
        type: 'GET',
        url: URL+'/svc.sp/consultarPlacaTerceiroPre.php',
        dataType: 'json',
        crossDomain: true,
        success: function(json)
        {
            if(json.id_erro == '0') {
                localStorage.setItem("sp:consulta_terceiro:token", JSON.stringify(json.token));
                url = json.token.captcha_url;
                $("div#img_captcha_sp").append('<img id="img_captcha_sp" src="'+url+'" />');
                $("div#img_captcha_sp span.loader").show();
                $("img#img_captcha_sp").one('load', function()
                {
                   $("div#img_captcha_sp span.loader").hide();
                }).each(function() {
                   if(this.complete)
                      $(this).load();
                });
            }
        }
     });
}

function add_img_pr()
{
    var url = URL+"/svc.pr/veiculo.auth.php?"+rand_string();

    $("div#img_captcha_pr img").remove();
    $("div#img_captcha_pr").append('<img id="img_captcha_pr" src="'+url+'" />');

    $("div#img_captcha_pr span.loader").show();

    $("img#img_captcha_pr").one('load', function()
    {
        $("div#img_captcha_pr span.loader").hide();
    }).each(function() {
        if(this.complete)
            $(this).load();
    });
}

function add_img_rs()
{
    var url = URL+"/svc.rs/veiculo.auth.php?"+rand_string();

    $("div#img_captcha_rs img").remove();
    $("div#img_captcha_rs").append('<img id="img_captcha_rs" src="'+url+'" />');

    $("div#img_captcha_rs span.loader").show();

    $("img#img_captcha_rs").one('load', function()
    {
        $("div#img_captcha_rs span.loader").hide();
    }).each(function() {
            if(this.complete)
                $(this).load();
        });
}

function add_img_ba()
{
    var url = URL+"/svc.ba/veiculo.auth.php?"+rand_string();

    $("div#img_captcha_ba img").remove();
    $("div#img_captcha_ba").append('<img id="img_captcha_ba" src="'+url+'" />');

    $("div#img_captcha_ba span.loader").show();

    $("img#img_captcha_ba").one('load', function()
    {
        $("div#img_captcha_ba span.loader").hide();
    }).each(function() {
            if(this.complete)
                $(this).load();
        });
}



function add_img_rj(n)
{
   if(n == 1)
      var url = URL+"/svc.rj/veiculo.auth.php?"+rand_string();
   else
      var url = URL+"/svc.rj/multa.auth.php?"+rand_string();

   $("div#img_captcha"+n+"_rj img").remove();
   $("div#img_captcha"+n+"_rj").append('<img class="ui-corner-all" id="img_captcha'+n+'_rj" src="'+url+'" />');

   $("div#img_captcha"+n+"_rj span.loader").show();

   $("img#img_captcha"+n+"_rj").one('load', function()
   {
      $("div#img_captcha"+n+"_rj span.loader").hide();
   }).each(function() {
      if(this.complete)
         $(this).load();
   });
}

function valida_campo_descricao()
{
   if($("select[name=salvar]").val() == 'true' && !$("input[name=descricao]").val())
   {
      msg_erro("O campo 'Descrição' está em branco!", 'popup_erro');
      return false;
   }

   return true;
}


function valida_form_branco_mg()
{
   var ok = true;
   var chassi = $("input[name=chassi-mg]").val();
   var placa = $("input[name=placa-mg]").val();
   var captcha = $("input[name=captcha-mg]").val();

   if(!placa)
   {
      msg_erro("O campo 'Placa' está em branco!", 'popup_erro');
      ok = false;
   }
   if(!valida_placa(placa))
   {
      msg_erro("O formato da placa é inválido.", 'popup_erro');
      ok = false;
   }
   else if(!chassi)
   {
      msg_erro("O campo 'Chassi' está em branco!", 'popup_erro');
      ok = false;
   }
   else if(!captcha)
   {
      msg_erro("O conteúdo da imagem deve ser informado!", 'popup_erro');
      ok = false;
   }
   else if(captcha.length != 6)
   {
      msg_erro("O conteúdo da imagem deve conter 6 caracteres!", 'popup_erro');
      ok = false;
   }

   return ok;
}

function valida_form_branco_sp()
{
    var ok = true;
    var renavan = $("input[name=renavan-sp]").val();
    var placa = $("input[name=placa-sp]").val();
    var captcha = $("input[name=captcha-sp]").val();
    var usuario = $("#usuario-sp").val();

    if(!renavan) {
      msg_erro("O campo 'Renavam' está em branco!", 'popup_erro');
      ok = false;
   }
   else if(!placa)
   {
      msg_erro("O campo 'Placa' está em branco!", 'popup_erro');
      ok = false;
   }
   else if(!valida_placa(placa))
   {
      msg_erro("O formato da placa é inválido.", 'popup_erro');
      ok = false;
   } else if(!captcha) {
      msg_erro("O conteúdo da imagem deve ser informado!", 'popup_erro');
      ok = false;
   }

   return ok;
}

function valida_form_branco_rj()
{
   var ok = true;
   var renavan = $("input[name=renavan-rj]").val();
   var placa = $("input[name=placa-rj]").val();
   var captcha1 = $("input[name=captcha1-rj]").val();
   var captcha2 = $("input[name=captcha2-rj]").val();

   if(!placa)
   {
      msg_erro("O campo 'Placa' está em branco!", 'popup_erro');
      ok = false;
   }
   if(!valida_placa(placa))
   {
      msg_erro("O formato da placa é inválido.", 'popup_erro');
      ok = false;
   }
   else if(!renavan)
   {
      msg_erro("O campo 'Renavam' está em branco!", 'popup_erro');
      ok = false;
   }
   else if(!captcha1 || !captcha2)
   {
      msg_erro("O conteúdo da imagem deve ser informado!", 'popup_erro');
      ok = false;
   }
   else if(captcha1.length != 4 || captcha2.length != 4)
   {
      msg_erro("O conteúdo da imagem possui 4 caracteres!", 'popup_erro');
      ok = false;
   }

   return ok;
}

function valida_form_branco_pe()
{
    var ok = true;
    var placa = $("input[name=placa-pe]").val();

    if(!placa)
    {
        msg_erro("O campo 'Placa' está em branco!", 'popup_erro');
        ok = false;
    }
    if(!valida_placa(placa))
    {
        msg_erro("O formato da placa é inválido.", 'popup_erro');
        ok = false;
    }

    return ok;
}

function valida_form_branco_pr()
{
    var ok = true;
    var renavan = $("input[name=renavan-pr]").val();
    var captcha = $("input[name=captcha-pr]").val();

    if(!renavan)
    {
        msg_erro("O campo 'Renavam' está em branco!", 'popup_erro');
        ok = false;
    }
    else if(!captcha)
    {
        msg_erro("O conteúdo da imagem deve ser informado!", 'popup_erro');
        ok = false;
    }
    else if(captcha.length != 6)
    {
        msg_erro("O conteúdo da imagem deve conter 6 caracteres!", 'popup_erro');
        ok = false;
    }

    return ok;
}

function valida_form_branco_rs()
{
    var ok = true;
    var placa = $("input[name=placa-rs]").val();
    var renavan = $("input[name=renavan-rs]").val();
    var captcha = $("input[name=captcha-rs]").val();

    if(!placa)
    {
        msg_erro("O campo 'Placa' está em branco!", 'popup_erro');
        ok = false;
    }
    else if(!valida_placa(placa))
    {
        msg_erro("O formato da placa é inválido.", 'popup_erro');
        ok = false;
    }
    else if(!renavan)
    {
        msg_erro("O campo 'Renavam' está em branco!", 'popup_erro');
        ok = false;
    }
    else if(!captcha)
    {
        msg_erro("O conteúdo da imagem deve ser informado!", 'popup_erro');
        ok = false;
    }

    return ok;
}

function valida_form_branco_ba()
{
    var ok = true;
    var renavan = $("input[name=renavan-ba]").val();
    var captcha = $("input[name=captcha-ba]").val();

    if(!renavan)
    {
        msg_erro("O campo 'Renavam' está em branco!", 'popup_erro');
        ok = false;
    }
    else if(!captcha)
    {
        msg_erro("O conteúdo da imagem deve ser informado!", 'popup_erro');
        ok = false;
    }

    return ok;
}



function callback_consveic_sp(erro, id_erro)
{

   if(id_erro != 0)
   {
      $.mobile.hidePageLoadingMsg();
      $("#btn_consultar").removeClass('ui-disabled');      
      msg_erro(erro, 'popup_erro');
   }
   else
   {
      
      localStorage.setItem('renavan', $("input[name=renavan-sp]").val());
      localStorage.setItem('estado', 'sp');

      $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
   }

}

function callback_consveic_terceiro_sp(erro, id_erro)
{
    if(id_erro != 0) {
        $.mobile.hidePageLoadingMsg();
        add_img_sp();
        $("#btn_consultar").removeClass('ui-disabled');
    }

    if(id_erro == 0) {
        localStorage.setItem('renavan', $("input[name=renavan-sp]").val());
        localStorage.setItem('estado', 'sp');
        $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
    }   else if (id_erro == '1') {
        msg_erro('Parametros inválidos', 'popup_erro');
    } else if (id_erro == '2') {
        msg_erro('Registro não encontrado', 'popup_erro');
    } else if (id_erro == '3') {
        msg_erro('Captcha incorreto.', 'popup_erro');
    } else {
        msg_erro('Erro inesperado.', 'popup_erro');
    }    

}

function callback_consveic_sp_foraform(erro, id_erro)
{
   if(id_erro != 0)
   {
      $.mobile.hidePageLoadingMsg();
      msg_erro(erro, 'popup_erro');
   }
   else
   {
      localStorage.setItem('renavan', renavan);
      localStorage.setItem('estado', 'sp');

      $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
   }
}

function callback_consveic_rj(erro, id_erro)
{
   if(id_erro != 0)
   {
      $.mobile.hidePageLoadingMsg();

      add_img_rj(1);
      add_img_rj(2);

      $("input[name=captcha1-rj]").val("");
      $("input[name=captcha2-rj]").val("");

      msg_erro(erro, 'popup_erro');
   }
   else
   {
      localStorage.setItem('placa', $("input[name=placa-rj]").val().toUpperCase());
      localStorage.setItem('estado', 'rj');

      $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
   }

}

function callback_consveic_mg(erro, id_erro)
{
    if(id_erro != 0)
    {
        $.mobile.hidePageLoadingMsg();
        add_img_mg();
        $("input[name=captcha-mg]").val("");
        msg_erro(erro, 'popup_erro');
    }
    else
    {
        localStorage.setItem('placa', $("input[name=placa-mg]").val().toUpperCase());
        localStorage.setItem('estado', 'mg');
        $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
    }
}

function callback_consveic_pr(erro, id_erro)
{
    if(id_erro != 0)
    {
        $.mobile.hidePageLoadingMsg();
        add_img_pr();
        $("input[name=captcha-pr]").val("");
        msg_erro(erro, 'popup_erro');
    }
    else
    {
        localStorage.setItem('renavan', $("input[name=renavan-pr]").val());
        localStorage.setItem('estado', 'pr');
        $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
    }
}

function callback_consveic_rs(erro, id_erro)
{
    if(id_erro != 0)
    {
        $.mobile.hidePageLoadingMsg();
        add_img_rs();
        $("input[name=captcha-rs]").val("");
        msg_erro(erro, 'popup_erro');
    }
    else
    {
        localStorage.setItem('placa', $("input[name=placa-rs]").val().toUpperCase());
        localStorage.setItem('renavan', $("input[name=renavan-rs]").val());
        localStorage.setItem('estado', 'rs');
        $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
    }
}

function callback_consveic_pe(erro, id_erro)
{
    if(id_erro != 0)
    {
        $.mobile.hidePageLoadingMsg();
        msg_erro(erro, 'popup_erro');
    }
    else
    {
        localStorage.setItem('placa', $("input[name=placa-pe]").val().toUpperCase());
        localStorage.setItem('estado', 'pe');
        $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
    }
}

function callback_consveic_pe_foraform(erro, id_erro)
{
    if(id_erro != 0)
    {
        $.mobile.hidePageLoadingMsg();
        msg_erro(erro, 'popup_erro');
    }
    else
    {
        localStorage.setItem('placa', placa);
        localStorage.setItem('estado', 'pe');
        $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
    }
}


function callback_consveic_ba(erro, id_erro)
{
    if(id_erro != 0)
    {
        $.mobile.hidePageLoadingMsg();
        add_img_ba();
        $("input[name=captcha-ba]").val("");
        msg_erro(erro, 'popup_erro');
    }
    else
    {
        localStorage.setItem('renavan', $("input[name=renavan-ba]").val());
        localStorage.setItem('estado', 'ba');
        $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
    }
}

function consulta_terceiro_sp_fn (placa, renavam, captcha, descricao, callback) {
    var token = JSON.parse(localStorage.getItem("sp:consulta_terceiro:token"));
    $.ajax({
        type: 'POST',
        url: URL+'/svc.sp/consultarPlacaTerceiro.php',
        dataType: 'json',
        data: {
            token: token,
            placa: placa,
            renavam: renavam,
            captcha: captcha
        },
        crossDomain: true,
        success: function(json) {
            if(json.id_erro == '0') {
                json.consulta_terceiro_sp = true;
                grava_dados('sp', renavam, placa, ' ', descricao, json, callback);    
            } else {
                callback(json.msg, json.id_erro);
            }
        }
    });
}


function consultar_veiculo()
{
   if(estado == 'mg' && valida_form_branco_mg() && valida_campo_descricao())
   {
      $.mobile.showPageLoadingMsg();
      $("#btn_consultar").addClass('ui-disabled');

      //estado, placa, chassi, renavan, descricao, captcha, callback
      consulta_hibrida(
         estado,
         $("input[name=placa-mg]").val().toUpperCase(),
         $("input[name=chassi-mg]").val(),
         '',
         $("input[name=descricao]").val(),
         $("input[name=captcha-mg]").val(),
         '',
         '',
         callback_consveic_mg
      );
   }
   else if(estado == 'sp' && valida_form_branco_sp() && valida_campo_descricao())
   {
        var usuario = $("#usuario-sp").val();
        $.mobile.showPageLoadingMsg();
        $("#btn_consultar").addClass('ui-disabled');
        cpf = cpf ? cpf : localStorage.setItem('cpf', cpf);
        senha = senha ? senha : localStorage.setItem('senha', senha);

       if(usuario) {

          //estado, placa, chassi, renavan, descricao, captcha, callback
          consulta_hibrida(
             estado,
             $("input[name=placa-sp]").val(),
             '',
             $("input[name=renavan-sp]").val(),
             $("input[name=descricao]").val(),
             '',
             cpf,
             senha,

             // Callback
             function(erro, id_erro) {

                /* 
                    Caso o o renavam nao pertenca
                    ao usuario logado a api retornara um erro (102).
                */
                if(id_erro == '102') {
                    consulta_terceiro_sp_fn (
                        $("input[name=placa-sp]").val(),
                        $("input[name=renavan-sp]").val(),
                        $("input[name=captcha-sp]").val(),
                        $("input[name=descricao]").val(),
                        callback_consveic_terceiro_sp
                    );                    
                } else {
                    callback_consveic_sp(erro, id_erro);
                }
             }
          );

        } else {
            consulta_terceiro_sp_fn (
                $("input[name=placa-sp]").val(),
                $("input[name=renavan-sp]").val(),
                $("input[name=captcha-sp]").val(),
                $("input[name=descricao]").val(),
                callback_consveic_terceiro_sp
            );
        }

   }

   else if(estado == 'rj' && valida_form_branco_rj() && valida_campo_descricao())
   {
      $.mobile.showPageLoadingMsg();
      $("#btn_consultar").addClass('ui-disabled');

      //estado, placa, chassi, renavan, descricao, captcha, callback
      consulta_hibrida(
         estado,
         $("input[name=placa-rj]").val().toUpperCase(),
         '',
         $("input[name=renavan-rj]").val(),
         $("input[name=descricao]").val(),
         '',
         '',
         '',
         callback_consveic_rj
      );
   }

   else if(estado == 'pr' && valida_form_branco_pr() && valida_campo_descricao())
   {
       $.mobile.showPageLoadingMsg();
       $("#btn_consultar").addClass('ui-disabled');

       //estado, placa, chassi, renavan, descricao, captcha, callback
       consulta_hibrida(
           estado,
           '',
           '',
           $("input[name=renavan-pr]").val(),
           $("input[name=descricao]").val(),
           $("input[name=captcha-pr]").val(),
           '',
           '',
           callback_consveic_pr
       );
   }

   else if(estado == 'rs' && valida_form_branco_rs() && valida_campo_descricao())
   {
       $.mobile.showPageLoadingMsg();
       $("#btn_consultar").addClass('ui-disabled');

       //estado, placa, chassi, renavan, descricao, captcha, callback
       consulta_hibrida(
           estado,
           $("input[name=placa-rs]").val().toUpperCase(),
           '',
           $("input[name=renavan-rs]").val(),
           $("input[name=descricao]").val(),
           $("input[name=captcha-rs]").val(),
           '',
           '',
           callback_consveic_rs
       );
   }


   else if(estado == 'pe' && valida_form_branco_pe() && valida_campo_descricao())
   {
       $.mobile.showPageLoadingMsg();

       //estado, placa, chassi, renavan, descricao, captcha, callback
       consulta_hibrida(
           estado,
           $("input[name=placa-pe]").val(),
           '',
           '',
           $("input[name=descricao]").val(),
           '',
           '',
           '',
           callback_consveic_pe
       );
   }

   else if(estado == 'ba' && valida_form_branco_ba() && valida_campo_descricao())
   {
       $.mobile.showPageLoadingMsg();
       $("#btn_consultar").addClass('ui-disabled');

       //estado, placa, chassi, renavan, descricao, captcha, callback
       consulta_hibrida(
           estado,
           '',
           '',
           $("input[name=renavan-ba]").val(),
           $("input[name=descricao]").val(),
           $("input[name=captcha-ba]").val(),
           '',
           '',
           callback_consveic_ba
       );
   }


}



