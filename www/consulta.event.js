﻿
var cpf;
var senha;

function inicializa_mg()
{
    $("div.cons").hide();
    $("div.cons_mg").show();

   add_img_mg();
}

function inicializa_pr()
{
    $("div.cons").hide();
    $("div.cons_pr").show();

    add_img_pr();
}

function inicializa_pe()
{
    $("div.cons").hide();
    $("div.cons_pe").show();
}

function inicializa_ba()
{
    $("div.cons").hide();
    $("div.cons_ba").show();

    add_img_ba();
}

function inicializa_rj()
{
    $("div.cons").hide();
    $("div.cons_rj").show();

    add_img_rj(1);
    add_img_rj(2);
}

function inicializa_sp()
{
    $("div.cons").hide();
    $("div.cons_sp").show();

    /* 
        Obtem o token necessario para consultar uma placa,
        e insere a imagem captcha na tela.
    */
    add_img_sp();

    estado = 'sp';
}

function inicializa_rs()
{
    $("div.cons").hide();
    $("div.cons_rs").show();

    add_img_rs();
}

function pg_consulta()
{
   inicializa_banner();

   descricao = localStorage.getItem('descricao');
   placa = localStorage.getItem('placa');
   chassi = localStorage.getItem('chassi');
   renavan = localStorage.getItem('renavan');
   cpf = localStorage.getItem('cpf');
   senha = localStorage.getItem('senha');

   if(!localStorage.getItem('estado'))
   {
       inicializa_sp();
       $("div.cons_sp").show();
   }

   else
      estado = localStorage.getItem('estado');

    if(estado != 'pe' && estado != 'sp')
        $("#btn_consultar").addClass('ui-disabled');

   localStorage.setItem('chassi', '');
   localStorage.setItem('placa', '');
   localStorage.setItem('renavan', '');
   localStorage.setItem('cpf', '');
   localStorage.setItem('senha', '');
   localStorage.setItem('descricao', '');

   if(placa || (renavan && renavan != 'null'))
   {
      $("#btn_consultar span.ui-btn-text").html("Consultar");

      if(estado == 'mg')
      {
         inicializa_mg();
         $("input[name=placa-mg]").val(placa.toUpperCase());
         $("input[name=chassi-mg]").val(chassi);
         $("input[name=descricao]").val(descricao);
      }
      else if(estado == 'rj')
      {
          inicializa_rj();
          $("input[name=placa-rj]").val(placa.toUpperCase());
          $("input[name=renavan-rj]").val(renavan);
          $("input[name=descricao]").val(descricao);
      }
      else if(estado == 'pr')
      {
          inicializa_pr();
          $("input[name=renavan-pr]").val(renavan);
          $("input[name=descricao]").val(descricao);
      }
      else if(estado == 'ba')
      {
          inicializa_ba();
          $("input[name=renavan-ba]").val(renavan);
          $("input[name=descricao]").val(descricao);
      }
      else if(estado == 'rs')
      {
          inicializa_rs();
          $("input[name=placa-rs]").val(placa.toUpperCase());
          $("input[name=renavan-rs]").val(renavan);
          $("input[name=descricao]").val(descricao);
      }

      $("div.field_preenchida").hide();
   }

   var habilitar_btn_consulta = function() {
      $("#btn_consultar").removeClass('ui-disabled');
   }

   $("input[name=captcha-mg], input[name=captcha1-rj], input[name=captcha2-rj], input[name=captcha-pr], input[name=captcha-ba], input[name=captcha-rs]")
    .bind("keypress", habilitar_btn_consulta)
    .bind("keyup", habilitar_btn_consulta)
    .bind("keydown", habilitar_btn_consulta);

   // botão de consultar veiculo
	$("#btn_consultar").click(consultar_veiculo);

   $("#salvar").change(function( event, ui )
   {
      if($(this).val() == 'true')
      {
         $("#btn_consultar span.ui-btn-text").html("Incluir");
         $("div.pg_consulta div.field_descricao").show();
      }
      else
      {
         $("#btn_consultar span.ui-btn-text").html("Consultar");
         $("div.pg_consulta div.field_descricao").hide();
      }
   });
   
   $("select[name=estado]").change(function(){

      if($(this).val() == estado)
         return;

      estado = $(this).val();

       if(estado != 'pe')
       {
           $("#btn_consultar").addClass('ui-disabled');
       }
       else
       {
           $("#btn_consultar").removeClass('ui-disabled');
       }

      eval("inicializa_"+estado+"();");

   });

   if(localStorage.getItem('origin') == 'config.sp')
   {
      $('input:radio[name="estado"]').filter('[value="sp"]').parent().find("label[for].ui-btn").click()
      localStorage.setItem('origin', '');
   }

   $("#atualiza-captcha1-rj").click(function(){
      add_img_rj(1);
   });

   $("#atualiza-captcha2-rj").click(function(){
      add_img_rj(2);
   });

   $("#atualiza-captcha-mg").click(function(){
      add_img_mg();
   });

    $("#atualiza-captcha-ba").click(function(){
        add_img_ba();
    });

    $("#atualiza-captcha-pr").click(function(){
        add_img_pr();
    });

    $("#atualiza-captcha-rs").click(function(){
        add_img_rs();
    });

    $("#atualiza-captcha-sp").click(function(){
        add_img_sp();
    });
    

    // Popular o menu dropdown de usuario com usuario obtidos no banco local.
   var tmp;
   sql.query("select cpf, descricao, senha from usuario_sp", function(tx, rs)
   {
      for (var i=0; i < rs.rows.length; i++)
      {
         tmp = (cpf == rs.rows.item(i)['cpf'] ) ? 'selected="selected"' : '';

         $("#usuario-sp").append('<option '+tmp+' senha="'+rs.rows.item(i)['senha']+'" value="'+rs.rows.item(i)['cpf']+'">'+rs.rows.item(i)['descricao']+'</option>');

      }

      $("#usuario-sp").change(function(){
            var usuarioItem = $("#usuario-sp option:selected");
            localStorage.setItem('cpf', usuarioItem.val());
            localStorage.setItem('senha', usuarioItem.attr("senha"));  
            cpf = localStorage.getItem('cpf');
            senha = localStorage.getItem('senha');                            
      });


      $("#usuario-sp").selectmenu('refresh', true);
   });

}


