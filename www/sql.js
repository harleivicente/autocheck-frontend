﻿/*
classe de conexao web sql

Exemplo de consulta:

   sql.query("select placa, chassi from veiculo", function(tx, rs){
      console.log(rs.rows.item(0)['chassi']);
   });

Gerando um auto-incremento

   sql.auto_incrment('veiculo', 'id', function(id){
      console.log(id); // maior chave id + 1
   });

*/

// objeto central
var sql = {};

// variavel de conexao
sql.db = null;

sql.verbose = false;

// funcao geral de erro
sql.onError = function(tx, e) {
  alert("Ocorreu um erro na transacao: " + e.message);
}

// abre a conexao
sql.open = function() {
  var dbSize = 1 * 1024 * 1024; // 1MB
  sql.db = window.openDatabase("detran_app", "1.0", "Multas Detran", dbSize);
}

// executa as consultas
sql.query = function(query, callback)
{
   query_DEBUG = query;
   
   if(sql.verbose)
   {
	   console.log("SQL: "+query_DEBUG);
   }
   
   sql.db.transaction(function(tx) {
      tx.executeSql(query, [], callback, sql.onError);
   });
}

// gera auto-increment para a tabela t da chave col
sql.auto_incrment = function(t, col, callback)
{
   sql.query('SELECT max('+col+') as id FROM ' + t, function(tx, rs)
   {
	   if(!rs.rows.item(0)['id']){
		   callback( 1 );
	   }
	   else
	   {
		   callback( parseInt(rs.rows.item(0)['id'])+1 );		   
	   }

   });

}


/*
   Usada para gravar dados - tratar quando eh necessario INSERT ou UPDATE automaticamente

   t    = tabela
   key  = chave da tupla
   val  = valor para comparar a tupla
   cols = tuplas para inserir/atualizar
   callback = chamado quando tudo terminar

   OBS! O auto incremento so vai funcionar para tabelas onde a PK tenha o nome de ID

*/

sql.gravar = function(t, key, val, cols, callback)
{
   sql.query("select "+key+" from veiculo where "+key+" = '"+val+"'", function(tx, rs)
   {
      var s;      rs.rows.length
      if(rs.rows.length != 0) // registro existe! vai ter que dar update
      {
         s = squel.update();
         s.table(t);
         s.where(key+" = '"+val+"'");
      }
      else // dar insert
      {
         s = squel.insert();
         s.into(t);
      }

      // Montar campos na query
      for (var i in cols)
      {
         s.set(i, cols[i]);
      }

      if(rs.rows.length != 0) // update
      {
         sql.query(s.toString(), function(tx, rs) {
            callback();
         });
      }
      else // insert
      {
         sql.auto_incrment(t, 'id', function(id) {
            s.set('id', id);
            sql.query(s.toString(), function(tx, rs) {
               callback();
            });
         });
      }

   });
}






