﻿function pg_ls_veiculo()
{
   inicializa_banner();

   localStorage.setItem('navbar_ativa', 'veiculo');

   // Carega a listagem de veiculos salva
   sql.db.transaction(function(tx) {
      tx.executeSql("SELECT estado, placa, chassi, renavan, cpf, descricao FROM veiculo where descricao is not null and descricao <> ''", [], carrega_listagem_veiculos, sql.onError);
   });

   $("#popup_excluir").bind({
      popupafteropen: function(event, ui) {

         top_old = Math.round(($(window).height() - $(this).height()) / 4);
         left_old = Math.round(($(window).width() - $(this).width()) / 4);

         $(this).css("top", top_old);
         $(this).css("left", left_old);
      }
   });

   $("#btn_novo").unbind("click").click(function(){

   	if(!tah_online())
   	{
   		msg_erro("Habilite a conexão com a Internet para fazer a consulta", 'popup_erro');
   	}
   	else
    {
         localStorage.setItem('placa', '');
         localStorage.setItem('chassi', '');
         localStorage.setItem('estado', '');
         localStorage.setItem('cpf', '');
         localStorage.setItem('renavan', '');
         localStorage.setItem('descricao', '');

         $.mobile.changePage( "consulta.html", { transition: "none", changeHash: true });
    }
   });
}

// chamado quando a carrega_listagem_veiculos termina de executar
function eventos_botoes_veiculo()
{
   $("div#popup_excluir a.sim").click(function(){

      $.mobile.showPageLoadingMsg();

      sql.query("delete from veiculo where placa = '"+localStorage.getItem('placa')+"'", function(tx, rs){

         pg_ls_veiculo();
         $("#popup_excluir").popup("close");
         $.mobile.hidePageLoadingMsg();

      });

   });

   $("div#popupMenu a.excluir").click(function()
   {
      $("span.placa_exclusao").html(formata_placa( localStorage.getItem('placa') ));
   });

   $("ul.lv_veiculos li.veiculo").click(function(){

    placa = $('a.veic', this).attr('placa');
    descricao = $('a.veic', this).attr('descricao');
    chassi = $('a.veic', this).attr('chassi');
    renavan = $('a.veic', this).attr('renavan');
    cpf = $('a.veic', this).attr('cpf');
    estado = $('a.veic', this).attr('estado');

    localStorage.setItem('descricao', descricao);
    localStorage.setItem('chassi', chassi);
    localStorage.setItem('placa', placa);
    localStorage.setItem('renavan', renavan);
    localStorage.setItem('cpf', cpf);
    localStorage.setItem('estado', estado);

   });

   $("ul.lv_veiculos li.veiculo a.veic").click(function(){

      $.mobile.showPageLoadingMsg();

      sql.query("select data_atualizacao, placa, renavan, cpf, estado, descricao from veiculo where placa = '"+ $(this).attr('placa') +"'", function(tx, rs)
      {
         // Já consultou hoje - já fez cache da página
         if(rs.rows.item(0)['data_atualizacao'] == get_data() || !tah_online())
         {
            $.mobile.changePage( "veiculo.html", { transition: "none", changeHash: true });
         }
         else
         {
            if(rs.rows.item(0)['estado'] == 'pe')
            {
                //estado, placa, chassi, renavan, descricao, captcha, cpf, senha, callback
                consulta_hibrida(
                    rs.rows.item(0)['estado'],
                    rs.rows.item(0)['placa'],
                    '',
                    '',
                    rs.rows.item(0)['descricao'],
                    '',
                    '',
                    '',
                    callback_consveic_pe_foraform
                );
            }
            else if(rs.rows.item(0)['estado'] == 'sp') {


               sql.query("select cpf, descricao, senha from usuario_sp where cpf = '"+rs.rows.item(0)['cpf']+"'", function(tx2, rs2)
               {
                  if(!rs2.rows.length) {
                     msg_erro("Nenhum usuário cadastrado para o CPF '"+rs.rows.item(0)['cpf']+"'.", 'popup_erro');
                     $.mobile.hidePageLoadingMsg();
                     return;
                  }

                  consulta_hibrida(
                      rs.rows.item(0)['estado'],
                      rs.rows.item(0)['placa'],
                      '',
                      rs.rows.item(0)['renavan'],
                      rs.rows.item(0)['descricao'],
                      '',
                      rs2.rows.item(0)['cpf'],
                      rs2.rows.item(0)['senha'],
                      callback_consveic_sp_foraform
                  );
               });

            }
            else
            {
                $.mobile.changePage( "consulta.html", { transition: "none", changeHash: true });
            }
         }
      });
   });
}


function carrega_listagem_veiculos(tx, rs)
{
   // limpar a lista antes de carregar
   $("ul.lv_veiculos li.veiculo").remove();

   var saida = "";

   for (var i=0; i < rs.rows.length; i++)
   {
      saida += '<li class="veiculo"><a class="veic" descricao="'+rs.rows.item(i)['descricao']+'" chassi="' + rs.rows.item(i)['chassi']
               + '" id="'+rs.rows.item(i)['id']
               +'" placa="'+ rs.rows.item(i)['placa']
               +'" estado="'+ rs.rows.item(i)['estado']
               +'" renavan="'+ rs.rows.item(i)['renavan']
               +'" cpf="'+ rs.rows.item(i)['cpf']
               +'" href="javascript:void(0)">'+formata_placa(rs.rows.item(i)['placa'])+' - '+rs.rows.item(i)['descricao']+'</a>';

      saida += '<a class="edit" href="#popupMenu" data-rel="popup" data-theme="c" data-transition="none">Editar Veículo</a></li>';
   }

   if(rs.rows.length == 0)
   {
      saida += '<li class="veiculo semicone"><a href="#">Nenhum veículo cadastrado</a></li>';
   }

   $('ul.lv_veiculos').append(saida).listview('refresh');

   // eventos ao clicar nos botoes de veiculos aqui criados
   eventos_botoes_veiculo();
}

