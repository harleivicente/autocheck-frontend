﻿function pg_ls_pontos()
{
   inicializa_banner();

   sql.query("SELECT cnh, estado, cpf, data_nascimento, data_habilitacao, apelido FROM condutor WHERE apelido <> ''", carrega_listagem_condutores);

   $("#btn_nova_carteira").unbind("click").click(function(){

   	if(!tah_online())
   	{
   		msg_erro("Habilite a conexão com a Internet para fazer a consulta", 'popup_erro2');
   	}
   	else
		{
   		localStorage.setItem('cnh', '');
         localStorage.setItem('estado', '');
         localStorage.setItem('cpf', '');

   		$.mobile.changePage( "consulta-pontos.html", { transition: "none", changeHash: true });
		}

   });

   $("div#popupMenu a.excluir").bind({
      popupafteropen: function(event, ui) {
         $("span.condutor_exclusao", this).html(localStorage.getItem('apelido'));
      }
   });

   $("#popup_excluir_condutor a.sim").click(function(e){

      $.mobile.showPageLoadingMsg();

      e.preventDefault();
      sql.query("delete from condutor where cnh = '"+localStorage.getItem('cnh')+"'", function(tx, rs){

         pg_ls_pontos();
         $("#popup_excluir_condutor").popup("close");
         $.mobile.hidePageLoadingMsg();

      });
   });

}


function eventos_botoes_condutor()
{
   $("ul.lv_condutores li.condutor").click(function(){

      localStorage.setItem('cnh', $('a.condut', this).attr('cnh'));
      localStorage.setItem('estado', $('a.condut', this).attr('estado'));
      localStorage.setItem('cpf', $('a.condut', this).attr('cpf'));
      localStorage.setItem('apelido', $('a.condut', this).attr('apelido'));

   });

   $("li.condutor a.condut").click(function()
   {
      estado = $(this).attr('estado');
      cnh = $(this).attr('cnh');
      cpf = $(this).attr('cpf');
      data_habilitacao = $(this).attr('data_habilitacao');
      data_nascimento = $(this).attr('data_nascimento');
      apelido = $(this).html();

      $.mobile.showPageLoadingMsg();

      sql.query("SELECT data_atualizacao FROM condutor WHERE cnh = '"+ $(this).attr('cnh') +"'", function(tx, rs)
      {
         // Já consultou hoje - já fez cache da página
         if(rs.rows.item(0)['data_atualizacao'] == get_data() || !tah_online())
         {
            $.mobile.changePage("pontuacao.html", { transition: "none", changeHash: true });
         }
         else
         {
            if(estado == 'mg')
            {
               consultar_pontos(estado, cnh, '', '', data_habilitacao, data_nascimento, '', apelido, function(erro, id_erro)
               {
                  $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
               });
            }
            else if(estado == 'pe')
            {
                consultar_pontos(estado, cnh, '', '', '', '', '', apelido, function(erro, id_erro)
                {
                    $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
                });
            }
            else if(estado == 'sp')
            {
               sql.query("select cpf, descricao, senha from usuario_sp where cpf = '"+cpf+"'", function(tx2, rs2) {

                  if(!rs2.rows.length) {
                     msg_erro("Nenhum usuário cadastrado para o CPF '"+cpf+"'.", 'popup_erro');
                     $.mobile.hidePageLoadingMsg();
                     return;
                  }

                  consultar_pontos(estado, '', cpf, rs2.rows.item(0)['senha'], '', '', '', apelido, function(erro, id_erro)
                  {
                     $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
                  });

               });
            }

            else
            {
               $.mobile.changePage( "consulta-pontos.html", { transition: "none", changeHash: true });
            }

         }
      });

   });

}


function carrega_listagem_condutores(tx, rs)
{
    // limpar a lista antes de carregar
    $("ul.lv_condutores li.condutor").remove();

    var saida = "";

    if(rs.rows.length == 0)
    {
        saida += '<li class="condutor semicone"><a href="#">Nenhum condutor cadastrado</a></li>';
    }
    else
    {
        for(var indice = 0; indice < rs.rows.length ; indice++)
        {
//                saida = "";
//
//            alert('senha = '+senha);

                saida += '<li class="condutor"><a class="condut" data_habilitacao="'+rs.rows.item(indice)['data_habilitacao']+
                    '" data_nascimento="'+rs.rows.item(indice)['data_nascimento']+
                    '" apelido="'+rs.rows.item(indice)['apelido']+
                    '" cnh="' + rs.rows.item(indice)['cnh']+
                    '" estado="' + rs.rows.item(indice)['estado']+
                    '" cpf="' + rs.rows.item(indice)['cpf']+
                    '" href="javascript:void(0)">'+rs.rows.item(indice)['apelido']+'</a>';

                saida += '<a class="edit" href="#popupMenu" data-rel="popup" data-theme="c" data-transition="none">Condutor</a></li>';


        }

    }

    $('ul.lv_condutores').append(saida).listview('refresh');

    eventos_botoes_condutor();
}

