﻿

function monta_detalhe_condutor(json)
{
    var thead = '';
    var tbody = '';
    var table = '';

    for(var k in json.condutor)
    {
        if(k == 'Nome')
        {
            thead += '<th></th>';

            tbody += '<td class="titulo condutor"><b>' + json.condutor[k] + '</b></td>';
        }
        else
        {
            thead += '<th>'+k+'</th>';
            tbody += '<td>'+json.condutor[k]+'</td>';
        }
    }

    thead += '<th>Pontos</th>';
    tbody += '<td class="total_pontos" style="color:#CC0000; font-weight: bolder">'+json.total_pontos+'</td>';

    table += '<table data-role="table" class="detalhe condutor ui-body-d ui-shadow table-stroke">';
    table += '<thead><tr>';
    table += thead;
    table += '</tr></thead><tbody><tr>';
    table += tbody;
    table += '</tr></tbody></table><br>';

    $("div.pg_pontuacao").append(table).trigger('create');
}


function monta_detalhe_pontuacao(json)
{
    var thead = '';
    var tbody = '';
    var table = '';

    for(var k in json.pontos)
    {
        thead = '';
        tbody = '';

        for(var i in json.pontos[k])
        {
            if(i == 'Infração')
            {
                thead += '<th></th>';
                tbody += '<td class="titulo"><b>' + json.pontos[k][i] + '</b></td>';
            }
            else
            {
                thead += '<th>'+i+'</th>';
                tbody += '<td>'+json.pontos[k][i]+'</td>';
            }
        }

        table += '<table data-role="table" class="detalhe multa ui-body-d ui-shadow table-stroke">';

        table += '<thead><tr>';

        table += thead;

        table += '</tr></thead><tbody><tr>';

        table += tbody;

        table += '</tr></tbody></table><br>';
    }



    $("div.pg_pontuacao").append(table).trigger('create');

}
