﻿


function pg_pontos()
{
   inicializa_banner();

   cnh = localStorage.getItem('cnh');
   cpf = localStorage.getItem('cpf');
   senha = localStorage.getItem('senha');
   apelido = localStorage.getItem('apelido');
   estado = localStorage.getItem('estado');

   // Estados que não precisam de captcha
   if(estado == 'pe' || estado == 'mg' || estado == 'sp') {
      $("#btn_consultar").addClass('ui-disabled');
   }

   if(!localStorage.getItem('estado'))
      inicializa_pts_sp();

   localStorage.setItem('cnh', '');
   localStorage.setItem('estado', '');
   localStorage.setItem('renavan', '');
   localStorage.setItem('cpf', '');
   localStorage.setItem('senha', '');
   localStorage.setItem('apelido', '');

   if(cnh && estado == 'rj')
   {
      $("#btn_consultar span.ui-btn-text").html("Consultar");
      $("input[name=cnh]").val(cnh);
      $("input[name=cpf]").val(cpf);
      $("input[name=apelido]").val(apelido);

      inicializa_pts_rj();

      $("div.field_preenchida").hide();
   }
   else if(cnh && estado == 'pr')
   {
       $("#btn_consultar span.ui-btn-text").html("Consultar");
       $("input[name=cnh]").val(cnh);
       $("input[name=cpf]").val(cpf);
       $("input[name=apelido]").val(apelido);

       inicializa_pts_pr();

       $("div.field_preenchida").hide();
   }
   else if(cnh && estado == 'ba')
   {
       $("#btn_consultar span.ui-btn-text").html("Consultar");
       $("input[name=cnh]").val(cnh);
       $("input[name=apelido]").val(apelido);

       inicializa_pts_ba();

       $("div.field_preenchida").hide();
   }



   $("#salvar").change(function( event, ui )
   {
      if($(this).val() == 'true')
      {
         $("#btn_consultar span.ui-btn-text").html("Incluir");
         $("div.field_descricao").show();
      }
      else
      {
         $("#btn_consultar span.ui-btn-text").html("Consultar");
         $("div.field_descricao").hide();
      }
   });

   var habilitar_btn_consulta = function() {
      $("#btn_consultar").removeClass('ui-disabled');
   }

   $("input[name=captcha1-rj], input[name=captcha2-rj], input[name=captcha-rj], input[name=captcha-pr], input[name=captcha-ba]").bind("keypress", habilitar_btn_consulta);
   $("input[name=captcha1-rj], input[name=captcha2-rj], input[name=captcha-rj], input[name=captcha-pr], input[name=captcha-ba]").bind("keyup", habilitar_btn_consulta);
   $("input[name=captcha1-rj], input[name=captcha2-rj], input[name=captcha-rj], input[name=captcha-pr], input[name=captcha-ba]").bind("keydown", habilitar_btn_consulta);

   $("#btn_consultar").click(function()
   {
      var cnh               = $("input[name=cnh]").val();
      var data_habilitacao  = $("input[name=data_habilitacao]").val();
      var data_nascimento   = $("input[name=data_nascimento]").val();
      var apelido           = $("input[name=apelido]").val();
      var captcharj         = $("input[name=captcha-rj]").val();
      var captchapr         = $("input[name=captcha-pr]").val();
      var captchaba         = $("input[name=captcha-ba]").val();
      var cpf_field         = $("input[name=cpf]").val();

      if(estado == 'mg' && valida_form_pontos_mg(cnh, data_habilitacao, data_nascimento))
      {
         $.mobile.showPageLoadingMsg();

         $("#btn_consultar").addClass('ui-disabled');

         consultar_pontos(estado, cnh, '', '', data_habilitacao, data_nascimento, '', apelido, function(erro, id_erro)
         {
            if(id_erro != 0 && id_erro != 1)
            {
               $.mobile.hidePageLoadingMsg();
               $("#btn_consultar").removeClass('ui-disabled');
               msg_erro(erro, 'popup_erro');               
            }
            else
            {
               localStorage.setItem('cnh', cnh);
               $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
            }


         });


      }
      else if(estado == 'sp' && valida_form_pontos_sp())
      {
         $.mobile.showPageLoadingMsg();
         $("#btn_consultar").addClass('ui-disabled');

         consultar_pontos(estado, '', cpf, senha, '', '', '', apelido, function(erro, id_erro)
         {
            if(id_erro != 0)
            {
               $("input[name=captcha-sp]").val("");
               $.mobile.hidePageLoadingMsg();
               msg_erro(erro, 'popup_erro');
               $("#btn_consultar").removeClass('ui-disabled');
            }
            else
            {
               localStorage.setItem('cpf', cpf);
               $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
            }


         });


      }
      else if(estado == 'rj' && valida_form_pontos_rj(cnh, cpf_field, captcharj))
      {
         $.mobile.showPageLoadingMsg();
         $("#btn_consultar").addClass('ui-disabled');

         consultar_pontos(estado, cnh, cpf_field, '', '', '', captcharj, apelido,  function(erro, id_erro)
         {
            if(id_erro != 0)
            {
               add_img_pts_rj();
               $("input[name=captcha-rj]").val("");
               $.mobile.hidePageLoadingMsg();
               msg_erro(erro, 'popup_erro');
               $("#btn_consultar").removeClass('ui-disabled');
            }
            else
            {
               localStorage.setItem('cnh', cnh);
               $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
            }


         });


      }
      else if(estado == 'pr' && valida_form_pontos_pr(cnh, cpf_field, data_habilitacao, captchapr))
      {
          $.mobile.showPageLoadingMsg();
          $("#btn_consultar").addClass('ui-disabled');

          consultar_pontos(estado, cnh, cpf_field, '', data_habilitacao, '', captchapr, apelido, function(erro, id_erro)
          {
              if(id_erro != 0)
              {
                  add_img_pts_pr();
                  $("input[name=captcha-pr]").val("");
                  $.mobile.hidePageLoadingMsg();
                  msg_erro(erro, 'popup_erro');
                 $("#btn_consultar").removeClass('ui-disabled');
              }
              else
              {
                  localStorage.setItem('cnh', cnh);
                  $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
              }


          });


      }
      else if(estado == 'ba' && valida_form_pontos_ba(cnh, captchaba))
      {
          $.mobile.showPageLoadingMsg();
          $("#btn_consultar").addClass('ui-disabled');

          consultar_pontos(estado, cnh, '', '', '', '', captchaba, apelido, function(erro, id_erro)
          {
              if(id_erro != 0)
              {
                  $("input[name=captcha-ba]").val("");
                  $.mobile.hidePageLoadingMsg();
                  msg_erro(erro, 'popup_erro');
                  $("#btn_consultar").removeClass('ui-disabled');
                  add_img_pts_ba();
              }
              else
              {
                  localStorage.setItem('cnh', cnh);
                  $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
              }


          });


      }
      else if(estado == 'pe' && valida_form_pontos_pe(cnh))
      {
          $.mobile.showPageLoadingMsg();
         $("#btn_consultar").addClass('ui-disabled');
          consultar_pontos(estado, cnh, '', '', '', '', '', apelido, function(erro, id_erro)
          {
              if(id_erro != 0)
              {
                  $.mobile.hidePageLoadingMsg();
                  msg_erro(erro, 'popup_erro');
                  $("#btn_consultar").removeClass('ui-disabled');
              }
              else
              {
                  localStorage.setItem('cnh', cnh);
                  $.mobile.changePage( "pontuacao.html", { transition: "none", changeHash: true });
              }

          });

      }



   });

   $("select[name=estado]").change(function(){

      if($(this).val() == 'mg' || $(this).val() == 'pe' || $(this).val() == 'sp')
         $("#btn_consultar").removeClass('ui-disabled');
      else
         $("#btn_consultar").addClass('ui-disabled');

      if($(this).val() && $(this).val() == estado)
         return;

      estado = $(this).val();

      eval("inicializa_pts_"+estado+"();");

   });

   if(localStorage.getItem('origin') == 'config.sp')
   {
      $('input:radio[name="estado"]').filter('[value="sp"]').parent().find("label[for].ui-btn").click()
      localStorage.setItem('origin', '');
   }

    $("#atualiza-captcha-rj").click(function(){
        add_img_pts_rj();
    });

    $("#atualiza-captcha-ba").click(function(){
        add_img_pts_ba();
    });

    $("#atualiza-captcha-pr").click(function(){
        add_img_pts_pr();
    });


   $("#usuario-sp").change(function(){
      cpf = $(this).val();
      senha = $("#usuario-sp option:selected").attr("senha");
   });

   var tmp;
   sql.query("select cpf, descricao, senha from usuario_sp", function(tx, rs)
   {
      for (var i=0; i < rs.rows.length; i++)
      {
         tmp = (cpf == rs.rows.item(i)['cpf'] ) ? 'selected="selected"' : '';

         $("#usuario-sp").append('<option '+tmp+' senha="'+rs.rows.item(i)['senha']+'" value="'+rs.rows.item(i)['cpf']+'">'+rs.rows.item(i)['descricao']+'</option>');

      }

      $("#usuario-sp").selectmenu('refresh', true);
   });


}
