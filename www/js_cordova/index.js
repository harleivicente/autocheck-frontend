

function initAd() {
		
	if ( window.plugins && window.plugins.AdMob ) {
		
		var ad_units = {
			ios : {
				banner: 'ca-app-pub-1364440404657863/5243867036',
				interstitial: 'ca-app-pub-1364440404657863/9674066633'
			},
			android : {
				banner: 'ca-app-pub-1364440404657863/5243867036',
				interstitial: 'ca-app-pub-1364440404657863/9674066633'
			},
			wp8 : {
				banner: 'ca-app-pub-1364440404657863/5243867036',
				interstitial: 'ca-app-pub-1364440404657863/9674066633'
			}
		};
		var admobid = "";
		if( /(android)/i.test(navigator.userAgent) ) {
			admobid = ad_units.android;
		} else if(/(iphone|ipad)/i.test(navigator.userAgent)) {
			admobid = ad_units.ios;
		} else {
			admobid = ad_units.wp8;
		}
		
		window.plugins.AdMob.setOptions( {
			publisherId: admobid.banner,
			interstitialAdId: admobid.interstitial,
			bannerAtTop: false, // set to true, to put banner at top
			overlap: true, // set to true, to allow banner overlap webview
			offsetTopBar: false, // set to true to avoid ios7 status bar overlap
			isTesting: false, // receiving test ad
			autoShow: true // auto show interstitial ad when loaded
		});
					
	} else {
		console.log('admob plugin not ready');
	}
}

var app = {
    // Application Constructor
    initialize: function() {		
        this.bindEvents();
    },
    
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.    
    bindEvents: function() {		
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
				
		// Google AdMob
		initAd();
		window.plugins.AdMob.createBannerView();

        document.addEventListener("backbutton", function(e){

			if($.mobile.activePage.is('#page0'))
			{
			    e.preventDefault();
			    navigator.app.exitApp();
			}
			else if($.mobile.activePage.is('#pg_veiculo'))
			{
			   e.preventDefault();
				$.mobile.changePage( "veiculo.ls.html", { transition: "none", changeHash: true });
			}
			else if($.mobile.activePage.is('#pg_pontuacao'))
			{
			   e.preventDefault();
				$.mobile.changePage( "pontuacao.ls.html", { transition: "none", changeHash: true });
			}
			else if($.mobile.activePage.is('#pg_ls_veiculo') || $.mobile.activePage.is('#pg_ls_pontos'))
			{
			   e.preventDefault();
				$.mobile.changePage( "index.html", { transition: "none", changeHash: true });
			}
			else
			{
			    navigator.app.backHistory();
			}
        	
        }, false);
    	
        app.receivedEvent('deviceready');
        
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
