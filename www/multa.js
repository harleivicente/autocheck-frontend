﻿

function add_multa(json)
{
    var tabela = '';
    var thead = '';
    var tbody = '';

    for(var multa in json)
    {

        thead = '';
        tbody = '';

        for(var campo in json[multa])
        {
            if(campo == 'Infração')
            {
                thead += '<th></th>';
                tbody += '<td><b>'+json[multa][campo]+'</b></td>';
            }
            else
            {
                thead += '<th>'+campo+'</th>';
                tbody += '<td>'+json[multa][campo]+'</td>';
            }

        }

        tabela += '<table data-role="table" class="multa detalhe ui-body-d ui-shadow table-stroke">';
        tabela += '<thead><tr>';
        tabela += thead;

        if(estado == 'mg') tabela += '<th>Boleto</th>';

        tabela += '</tr></thead><tbody><tr>';
        tabela += tbody;

        if(estado == 'mg' && json[multa]['Valor'])
        {
            tabela += '<td class="boleto">';
            tabela += '<button onclick="cod_barras(\''+placa+'\', \''+renavan+'\', \''+json[multa]['Processamento']+'\', \''+json[multa]['Valor']+'\');" data-inline="true" data-icon="custom">';
            tabela += 'Emitir</button></td>';
        }

        tabela += '</tr></tbody></table>';
    }

    $("div.pg_multa").append(tabela).trigger('create');
}


function cod_barras(placa, renavam, n, valor)
{
	if(!tah_online())
	{
		msg_erro("Habilite a conexão com a Internet para obter o código de barras", 'popup_erro');
		return;
	}	
	
   valor = valor.replace("R$ ", "").replace(",", "");

   $.mobile.showPageLoadingMsg();

   $.ajax({
      type: 'POST',
      url: URL+'/svc.mg/dae.php',
      dataType: 'json',
      data: {
         'placa': placa,
         'renavam': renavam,
         'n': n,
         'valor': valor
      },
      success: function(json) {

         $.mobile.hidePageLoadingMsg();
         $("#sucessoCodigoBarras").popup("open");
         $('input[name=codigo_barras]').val(json.codigo_barras);

         $("a.link_boleto").unbind("click").click(function(){
            if(eh_apk())
            {
               loadURL("http://docs.google.com/viewer?url="+json.url);
            }
            else
            {
               window.open("http://docs.google.com/viewer?url="+json.url);
            }
         });
      }
   });

}




