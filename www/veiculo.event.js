

var placa;
var estado;

function pg_veiculo()
{
   inicializa_banner();

   // buscar placa do parametro
   placa = localStorage.getItem('placa') ? localStorage.getItem('placa') : 'VAZIO';
   renavan = localStorage.getItem('renavan') ? localStorage.getItem('renavan') : 'VAZIO';

   estado = localStorage.getItem('estado');
   // banner_config();

   // evento de transicao das guias
   $(document).delegate('.ui-navbar a', 'click', function ()
   {
       $(this).addClass('ui-btn-active');
       $('.content_div').hide();
       $('#' + $(this).attr('data-href')).show();
   });

   carrega_detalhe_veiculo(placa);
    carrega_detalhe_imposto(placa);

   carrega_detalhe_orgao_multas(placa, function(){
      mostra_contagem_multas();
   });

   // evento que guarda o status da navbar que foi clicada
   $("div.veic_navbar li a").click(function(){
      localStorage.setItem('navbar_ativa' , $(this).attr("clkid"));
   });

   if(localStorage.getItem('origin') == "index")
   {
      localStorage.setItem('navbar_ativa', "veiculo");
   }

   $("div#"+localStorage.getItem('navbar_ativa')).show();
   $("div.veic_navbar li a[clkid="+localStorage.getItem('navbar_ativa')+"]").addClass('ui-btn-active');

}