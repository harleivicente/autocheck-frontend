﻿function valida_form_pontos_mg(cnh, data_habilitacao, data_nascimento)
{
    if(!cnh)
    {
        msg_erro("Preencha o número da carteira de motorista", 'popup_erro');
        return false;
    }
    if(!data_nascimento)
    {
        msg_erro("Informe a data de nascimento", 'popup_erro');
        return false;
    }
    if(!data_habilitacao)
    {
        msg_erro("Informe a data da primeira habilitação", 'popup_erro');
        return false;
    }
    if($("select[name=salvar]").val() == 'true' && !$("input[name=apelido]").val())
    {
        msg_erro("O campo 'Apelido' está em branco!", 'popup_erro');
        return false;
    }

    return true;
}

function valida_form_pontos_pe(cnh)
{
    if(!cnh)
    {
        msg_erro("Preencha o número da carteira de motorista", 'popup_erro');
        return false;
    }
    if($("select[name=salvar]").val() == 'true' && !$("input[name=apelido]").val())
    {
        msg_erro("O campo 'Apelido' está em branco!", 'popup_erro');
        return false;
    }

    return true;
}

function valida_form_pontos_rj(cnh, cpf, captcha)
{
   if(!cnh)
   {
      msg_erro("Preencha o número da carteira de motorista", 'popup_erro');
      return false;
   }
   if(!cpf)
   {
      msg_erro("Informe o número de CPF", 'popup_erro');
      return false;
   }
   if(!captcha)
   {
      msg_erro("Transcreva o conteúdo da imagem", 'popup_erro');
      return false;
   }
   if($("select[name=salvar]").val() == 'true' && !$("input[name=apelido]").val())
   {
      msg_erro("O campo 'Apelido' está em branco!", 'popup_erro');
      return false;
   }

   return true;
}

function valida_form_pontos_pr(cnh, cpf, validade, captcha)
{
    if(!cnh)
    {
        msg_erro("Preencha o número da carteira de motorista", 'popup_erro');
        return false;
    }
    if(!cpf)
    {
        msg_erro("Informe o número de CPF", 'popup_erro');
        return false;
    }
    if (!validade || validade.length < 10) {
        msg_erro("Informe uma data válida", 'popup_erro');
        return false;
    }
    if(!captcha)
    {
        msg_erro("Transcreva o conteúdo da imagem", 'popup_erro');
        return false;
    }
    if($("select[name=salvar]").val() == 'true' && !$("input[name=apelido]").val())
    {
        msg_erro("O campo 'Apelido' está em branco!", 'popup_erro');
        return false;
    }

    return true;
}

function valida_form_pontos_ba(cnh, captcha)
{
    if(!cnh)
    {
        msg_erro("Preencha o número da carteira de motorista", 'popup_erro');
        return false;
    }
    if(!captcha)
    {
        msg_erro("Transcreva o conteúdo da imagem", 'popup_erro');
        return false;
    }
    if($("select[name=salvar]").val() == 'true' && !$("input[name=apelido]").val())
    {
        msg_erro("O campo 'Apelido' está em branco!", 'popup_erro');
        return false;
    }

    return true;
}


function valida_form_pontos_sp()
{

   if(!$("#usuario-sp").val())
   {
      msg_erro("Selecione um usuário de acesso ao detran!", 'popup_erro');
      return false;
   }
   if($("select[name=salvar]").val() == 'true' && !$("input[name=apelido]").val())
   {
      msg_erro("O campo 'Apelido' está em branco!", 'popup_erro');
      return false;
   }

   return true;
}

function add_img_pts_rj()
{
    $("div#img_captcha_rj img").remove();

    var url = URL+"/svc.rj/pontuacao.auth.php?"+rand_string();

    $("div#img_captcha_rj").append('<img class="ui-corner-all" id="img_captcha_rj" src="'+url+'" />');

    $("div#img_captcha_rj span.selecione").hide();
    $("div#img_captcha_rj span.loader").show();

    $("img#img_captcha_rj").one('load', function()
    {
        $("div#img_captcha_rj span.loader").hide();
    }).each(function() {
            if(this.complete)
                $(this).load();
        });
}

function add_img_pts_pr()
{
    $("div#img_captcha_pr img").remove();

    var url = URL+"/svc.pr/pontuacao.auth.php?"+rand_string();

    $("div#img_captcha_pr").append('<img class="ui-corner-all" id="img_captcha_pr" src="'+url+'" />');

    $("div#img_captcha_pr span.selecione").hide();
    $("div#img_captcha_pr span.loader").show();

    $("img#img_captcha_pr").one('load', function()
    {
        $("div#img_captcha_pr span.loader").hide();
    }).each(function() {
        if(this.complete)
            $(this).load();
    });
}

function add_img_pts_ba()
{
    $("div#img_captcha_ba img").remove();

    var url = URL+"/svc.ba/pontuacao.auth.php?"+rand_string();

    $("div#img_captcha_ba").append('<img class="ui-corner-all" id="img_captcha_ba" src="'+url+'" />');

    $("div#img_captcha_ba span.selecione").hide();
    $("div#img_captcha_ba span.loader").show();

    $("img#img_captcha_ba").one('load', function()
    {
        $("div#img_captcha_ba span.loader").hide();
    }).each(function() {
        if(this.complete)
            $(this).load();
    });
}



function inicializa_pts_mg()
{
    $("div.cons").hide();
    $("div.cons_mg").show();
}

function inicializa_pts_pe()
{
    $("div.cons").hide();
    $("div.cons_pe").show();
}

function inicializa_pts_rj()
{
    add_img_pts_rj();

    $("div.cons").hide();
    $("div.cons_rj").show();
}

function inicializa_pts_pr()
{
    add_img_pts_pr();

    $("div.cons").hide();
    $("div.cons_pr").show();
}

function inicializa_pts_ba()
{
    add_img_pts_ba();

    $("div.cons").hide();
    $("div.cons_ba").show();
}


function inicializa_pts_sp()
{
   estado = 'sp';
   $("div.cons").hide();
   $("div.cons_sp").show();
}


function consultar_pontos(estado, cnh, cpf, senha, data_habilitacao, data_nascimento, captcha, apelido, callback)
{
    $.ajax({
        type: 'POST',
        url: URL + '/svc.'+estado+'/pontuacao.php',
        dataType: 'json',
        data: {
            'email': localStorage.getItem('email'),
            'cnh': cnh,
            'data_habilitacao': data_habilitacao,
            'data_nascimento': data_nascimento,
            'captcha': captcha,
            'cpf': cpf,
            'senha': senha
        },
        success: function(json)
        {

            if(json.id_erro == 0) // tudo OK
            {
                if(cnh != '')
                {
                    sql.query("DELETE FROM condutor WHERE cnh = '"+cnh+"'");
                }
                else
                {
                    sql.query("DELETE FROM condutor WHERE cpf = '"+cpf+"'");
                }

                var s = squel.insert();

                s.into("condutor");

                s.set('estado', estado);
                if(cnh)
                  s.set('cnh', cnh);
                else
                  s.set('cnh', json.cnh);
                s.set('cpf', cpf);
                s.set('data_nascimento', data_nascimento);
                s.set('data_habilitacao', data_habilitacao);
                s.set('apelido', apelido);
                s.set('total_pontos', json.total_pontos);
                s.set('data_atualizacao', get_data());

                s.set('json', JSON.stringify(json));

                sql.query(s.toString());

            }

            callback(json.erro, json.id_erro);

        }
    });

}
