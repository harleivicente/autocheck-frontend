﻿
var total_infracoes;

// Visualizacao diferente para casos de consultas de terceiros em SP.
var consulta_terceiro_sp;

function carrega_detalhe_orgao_multas(placa, callback)
{
    var json;
    total_infracoes = 0;
    sql.query("select json from veiculo where placa = '" + placa + "' or renavan = '"+renavan+"'", function(tx, rs)
    {
        var soma = 0; // soma das multas em reais
        var tipo;

        json = JSON.parse(rs.rows.item(0)['json']);
        if(json.consulta_terceiro_sp) {
            consulta_terceiro_sp = true;
        } else {
            consulta_terceiro_sp = false;
        }

        for(var tipo in json.infracao)
        {
            for (var orgao in json.infracao[tipo])
            {
                total_infracoes += conta_multas(json.infracao[tipo][orgao]);
                if(tipo == 'multas')
                {
                    soma += total_valor_multas(json.infracao[tipo][orgao]);
                }

                $("ul."+tipo).show();
                $("ul."+tipo).append('<li><a tipo="'+tipo+'" ' +
                    'id="' + orgao + '" href="javascript:void(0)" placa="' + placa + '">' +
                    orgao + ' <span class="ui-li-count">' +  conta_multas(json.infracao[tipo][orgao]) + '</span></a></li>');
            }

        }

        $("#consulta-terceiro-sp-table").remove();

        if(consulta_terceiro_sp) {
            $("#multas").append(
                '<table id="consulta-terceiro-sp-table" data-role="table" class="detalhe ui-body-d ui-shadow table-stroke ui-table ui-table-reflow"> <tbody><tr><td><center><b>Multas</b></center></td><td><b class="ui-table-cell-label">Total de multas</b>' + json.veiculo['Total: '] + '</td><td><b class="ui-table-cell-label">Obs:</b> Para informações mais detalhadas sobre multas de veículos de SP é necessário logar com usuário Detran SP. </td></tr></tbody></table>'
            );
        } else if(total_infracoes == 0) {
            $("div.content_div_multa").append('<h3>Sem multa/autuação!&nbsp;&nbsp; <img style="vertical-align: middle;" src="images/smile.png" /> </h3>')
        } else {
            // valor total das multas
            $("ul.multas").append('<li data-role="list-divider">Total <span style="float: right;">R$ ' + soma.toFixed(2) + '</span></li>');

            $('ul.multas, ul.autuacoes').listview('refresh');

            $("ul.multas a, ul.autuacoes a").click(function()
            {
                $.mobile.showPageLoadingMsg();

                id = $(this).attr('id');
                placa = $(this).attr('placa');
                tipo = $(this).attr('tipo');

                localStorage.setItem('placa' , placa);
                localStorage.setItem('orgao' , id);
                localStorage.setItem('tipo' , tipo);

                $.mobile.changePage( "multa.html", { transition: "none", changeHash: true });

            });
      }

      callback();
   });

}

function carrega_detalhe_veiculo(placa)
{
    var json;
    var thead = '';
    var tbody = '';
    var table = '';

    sql.query("select json from veiculo where placa = '" + placa + "' or renavan = '"+renavan+"'", function(tx, rs)
    {

        json = JSON.parse(rs.rows.item(0)['json']);

        for(var k in json.veiculo)
        {
            thead += '<th>'+k+'</th>';
            tbody += '<td>'+json.veiculo[k]+'</td>';
        }

        table += '<table data-role="table" class="detalhe ui-body-d ui-shadow table-stroke">';
        table += '<thead><tr>';
        table += thead;
        table += '</tr></thead><tbody><tr>';
        table += tbody;
        table += '</tr></tbody></table><br>';

        $("div#veiculo").append(table).trigger('create');

   });

}

function carrega_detalhe_imposto(placa)
{
    var json;
    var thead = '';
    var tbody = '';
    var table = '';

    sql.query("select json from veiculo where placa = '" + placa + "' or renavan = '"+renavan+"'", function(tx, rs)
    {
        json = JSON.parse(rs.rows.item(0)['json']);

        for(var imposto in json.imposto) // ipva, trlv, dpvat
        {
            thead = '';
            tbody = '';
            thead += '<th></th>';
            tbody += '<td><center><b>'+imposto+'</b></center></td>';

            for(var k in json.imposto[imposto]) // ipva 2014, 2013, 2012
            {
                thead += '<th>'+k+'</th>';
                tbody += '<td>'+json.imposto[imposto][k]+'</td>';
            }

            table += '<table data-role="table" class="detalhe ui-body-d ui-shadow table-stroke">';
            table += '<thead><tr>';
            table += thead;
            table += '</tr></thead><tbody><tr>';
            table += tbody;
            table += '</tr></tbody></table>';

        }

        $("div#impostos").append(table).trigger('create');

    });

}


function mostra_contagem_multas()
{
    // Nao mostrar a contagem de multa
    if(consulta_terceiro_sp) {
        $("span.countBubl").hide();
    } else {
        $("span.countBubl").show();

        if(total_infracoes == 0)
        {
           $("span.countBubl").css("background", "#14FF14"); // pinta de verde
        }
        else
        {
           $("span.countBubl").css("background", "#ed1d24"); // pinta de vermelho
        }
     
        $("span.countBubl").html(total_infracoes);
     
        pisca_contagem_multa();

    }

}


var intID;
var mostrar = 0;

function RepeatCall()
{
   if(mostrar % 2 == 0)
      $("span.countBubl").hide();
   else
      $("span.countBubl").show();


   if(mostrar > 6)
      clearInterval(intID);

   mostrar++;
}

function pisca_contagem_multa()
{
    mostrar = 0;
    intID = setInterval(RepeatCall, 400);
}




